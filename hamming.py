def seqn(a,b):
    res = []
    j = 0
    for i in range(a,b):
        if i%a==0:
            j += 1
        if j%2==1:
            res.append(i)
        else:
            continue
    return res

def rd(b):
    res = ""
    j = k = 0
    i = 1
    while j!=len(b):
        if k==(i-1):
            res += '_'
            i *= 2
        else:
            res += b[j]
            j += 1
        k += 1
    return res
  
def ch(b):
    if len(b)%2==0:
        if list(b).count("1")%2==0:
            return "0"
        else:
            return "1"
    else:
        if list(b).count("1")%2==0:
            return "1"
        else:
            return "0"

def hamming(b):
    res = ""
    temp = ""
    ham = "" 
    b2 = rd(b)
    j = k = 0
    while k!=len(b):
        l = seqn(2**k,len(b2)+1)
        if l==[]:
            break
        for m in l:
            res += b2[m-1]
        temp += ch(res)
        k += 1
        res = "" 
    for i in range(len(b2)):
        if b2[i]=="_":
            ham += temp[j]
            j += 1
        else:
            ham += b2[i]
    return ham


if __name__=="__main__":
    bit = input("Enter the bit: ")
    bn = {'0','1'}
    cr1= set(bit)
    if bn == cr1 or cr1 == {'0'} or cr1 == {'1'}:
        print(f"Hamming code will be: {hamming(bit)}")
    else:
        print("Invalid Number")
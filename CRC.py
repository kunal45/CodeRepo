def xor(gn, tr):
	result = []
	for i in range(1, len(tr)):
		if gn[i] == tr[i]:
			result.append('0')
		else:
			result.append('1')
 
	return ''.join(result)
 
 
def crc(data, gn):
 
	ln = len(gn)
	tr = data[0: ln]
	while ln < len(data):
		if tr[0] == '1':
			tr = xor(gn, tr) + data[ln]
		else:
			tr = xor('0'*ln, tr) + data[ln]
		ln = ln + 1
 
	if tr[0] == '1':
		tr = xor(gn, tr)
	else:
		tr = xor('0'*ln, tr)
	return tr
 
 
if __name__ == '__main__':
	data = input('Enter data: ')
	gn = input('Enter divisor: ')
	bn = {'0','1'}
	cr1= set(data)
	cr2=set(gn)
	if bn == cr1 or cr1 == {'0'} or cr1 == {'1'}:
		if bn == cr2 or cr2 == {'0'} or cr2 == {'1'}:
			r = len(gn) - 1
			new_data = data + '0'*(r)
			rem = crc(new_data, gn)
			encdata = data + rem
			print("\nSender's Side")
			print('Remainder:', rem)
			print('Encoded data : ', encdata)
			print("\nReceiver's Side")
			nrem = crc(encdata,gn)
			check = int(nrem)
			if check == 0:
				print(f"Enc's Remainder: {nrem}")
				print("Thus, No Error in Data")
				print(f"Decoded Message: {encdata[:-r]}")
			else:
				print("Data has error")
		else:
			print("Inavlid Data, Enter Binary Values")
	else:
		print("Inavlid Data, Enter Binary Values")
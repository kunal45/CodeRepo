import math

def minimax(l,d):
    while d>1:
        i ,a,n = 0,[],len(l)
        
        m= min if d%2==0 else max
        while i<n:
            a.append(m(l[i],l[i+1]))
            i += 2
        l = a
        d-=1
    return max(l)

scores = [-1,4,2,6,-3,-5,0,7] 
d = int(math.log2(len(scores)))
print(minimax(scores,d))
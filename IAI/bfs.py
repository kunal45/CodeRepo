graph = {
	'A': ['B', 'D'],
	'B': ['C', 'F'],
	'C': ['E', 'G'],
	'D': ['F'],
	'E': ['B', 'F'],
	'F': ['A'],
	'G': ['E'],
}


# def bfs(graph, start):
# 	visited, queue = [], [start]
# 	while queue:
# 		vertex = queue.pop(0)
# 		if vertex not in visited:
# 			visited.append(vertex)
# 			queue += [i for i in graph[vertex] if i not in visited]
# 	for i in visited:
# 		print(i, end=" ")

# visited = [] # List for visited nodes.
# queue = []     #Initialize a queue

def bfs(graph, node):
	visited ,queue = [],[]
	visited.append(node)
	queue.append(node)
	while queue:
		m = queue.pop(0) 
		print (m, end = " ") 
		for neighbour in graph[m]:
			if neighbour not in visited:
				visited.append(neighbour)
				queue.append(neighbour)

root = input("Enter the Root Node: ")
print("\nFollowing is the Breadth-First Search Result: ")
bfs(graph, root)
